<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>Demoapp</title>
    <script src="{{ mix('js/app.js') }}" defer></script>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div>
    <div class="container">
            @if ($blogs)
                @foreach($blogs as $item)
                <div class="row">
                    <section class="3u">
                        <header>
                            <h2>{{ $item['user_name'].' '. $item['created_at']['date']}}</h2>
                        </header>
                        <a href="#" class="image full"><img src="{{__DIR__}}/img/{{ $item['img'] }}" alt=""></a>
                        <p>{{ $item['body'] }}</p>

                    </section>
                </div>
                @endforeach
            @endif

    </div>
</div>
</body>
</html>
