<?php

namespace SashaAdm\Demoapp\Http\Controllers;

use Illuminate\Routing\Controller;

class DemoappController extends Controller
{
    public function index(){
        $blogs = json_decode(file_get_contents('http://localhost/demoapi/index.php'), true)['data'];

        return view('demoapp::index', compact('blogs'));
    }


}