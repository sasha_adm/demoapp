<?php
namespace SashaAdm\Demoapp;

use Illuminate\Support\ServiceProvider;

class DemoappServiceProvider extends ServiceProvider
{
    public function boot(){

        require_once __DIR__ . '/Http/routes.php';

        $this->loadViewsFrom(__DIR__ . '/Views', 'demoapp');

    }


}